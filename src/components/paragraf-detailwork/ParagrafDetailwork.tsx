import React from 'react'

interface ParagrafDetailWorkProps {
  title?: string
  subTitle?: string
  paragraf?: string
  styleParagraf?: string
}

function ParagrafDetailwork(props: ParagrafDetailWorkProps) {
  const {
    title,
    subTitle,
    paragraf,
    styleParagraf,
  } = props

  return (
    <div className="mb-6">
      <h3 className="text-red-500 text-xl">{subTitle}</h3>
      <h1 className="text-gray-600 text-4xl font-bold">{title}</h1>
      <p className={styleParagraf}>{paragraf}</p>
    </div>
  )
}

export default ParagrafDetailwork
